Terraform K8s fluentbit Module
=============================

Terraform module to create following K8S resources:
- DaemonSet
- Service
- Secret
- ConfigMap

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Licence](#licence)

## <a name="variables"></a> Required Input Variables
Module does not require any input variables. See [full list](variables.tf) of supported variables

## <a name="usage"></a> Usage

#Import the FluentBit module
```hcl-terraform
module "fluentbit" {
  source        = "./fluentbit_k8s/"
  fluentbt_host = "< elasticsearch host >"
  fluentbt_port = "< elasticsearch port >"
  fluentbt_index  = "my_index"
  fluentbt_logstash_prefix  = "my_pref"
  fluentbt_type  = "_doc"
  fluentbt_user  = "< elasticsearch user >"
  fluentbt_pass  = "< elasticsearch password >"
}
```

## <a name="license"></a> License
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Trisheckin <tva2002@gmail.com><br/>Ukraine