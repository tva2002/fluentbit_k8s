# Create a deployment for the service
resource "kubernetes_daemonset" "fluent-bit" {
#  count = ""

  metadata {
    name      = "fluent-bit"
    namespace = var.namespace
    labels = {
      k8s-app = "fluent-bit-logging"
      version = "v1"
      "kubernetes.io/cluster-service" = "true"
        }
  }

  spec {
    selector {
      match_labels = {
        k8s-app = "fluent-bit-logging"
      }
    }

    template {
      metadata {
      labels = {
          k8s-app = "fluent-bit-logging"
          version = "v1"
         "kubernetes.io/cluster-service" = "true"
        }

     annotations = {
        "prometheus.io/scrape" = "true"
        "prometheus.io/port" = "2020"
        "prometheus.io/path" = "/api/v1/metrics/prometheus"
        }


      }

      spec {
        service_account_name            = "fluent-bit"
    toleration {
      key      = "node-role.kubernetes.io/master"
      effect   = "NoSchedule"
      operator = "Exists"
               }

    toleration {
      effect   = "NoExecute"
      operator = "Exists"
               }

    toleration {
      effect   = "NoSchedule"
      operator = "Exists"
               }

#        automount_service_account_token = true
#        restart_policy                  = "Always"

        container {
          name                     = "fluent-bit"
          image                    = "${var.image_name}/${var.image_tag}"
          image_pull_policy        = var.image_pull_policy

           port {
            name           = "fluent-bit"
            container_port = 2020
            protocol       = "TCP"
          }

#          resources {
#            requests {
#              memory = var.resources_requests_memory
#              cpu    = var.resources_requests_cpu
#            }
#
#            limits {
#              memory = var.resources_limits_memory
#              cpu    = var.resources_limits_cpu
#            }
#          }

          env {
            name = "FLUENT_ELASTICSEARCH_HOST"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-host
            value_from {
              secret_key_ref {
                    key = "fluentbt_host"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          env {
            name = "FLUENT_ELASTICSEARCH_PORT"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-port
            value_from {
              secret_key_ref {
                    key = "fluentbt_port"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          env {
            name = "FLUENT_ELASTICSEARCH_INDEX"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-port
            value_from {
              secret_key_ref {
                    key = "fluentbt_index"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          env {
            name = "FLUENT_ELASTICSEARCH_TYPE"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-port
            value_from {
              secret_key_ref {
                    key = "fluentbt_type"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          env {
            name = "FLUENT_ELASTICSEARCH_USER"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-port
            value_from {
              secret_key_ref {
                    key = "fluentbt_user"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          env {
            name = "FLUENT_ELASTICSEARCH_PASSWORD"
#            value = kubernetes_secret.fluent-bit.data.fluentbt-port
            value_from {
              secret_key_ref {
                    key = "fluentbt_pass"
                    name = kubernetes_secret.fluent-bit.metadata.0.name
                }
            }
          }

          volume_mount {
            name       = "varlog"
            mount_path = "/var/log"
            }

	  volume_mount {
            name       = "varlibdockercontainers"
            mount_path = "/var/lib/docker/containers"
	    read_only  = "true"
            }

	  volume_mount {
            name       = "journal"
            mount_path = "/journal"
	    read_only  = "true"
            }

	  volume_mount {
            name       = "fluent-bit-config"
            mount_path = "/fluent-bit/etc/"
            }



 
#          liveness_probe {
#            initial_delay_seconds = 3
#            success_threshold     = 1
#            timeout_seconds       = 1
#
#            http_get {
#              path   = "/ambassador/v0/check_alive"
#              port   = 8877
#              scheme = "HTTP"
#            }
#          }

#          readiness_probe {
#            initial_delay_seconds = 3
#            success_threshold     = 1
#            timeout_seconds       = 1
#
#            http_get {
#              path   = "/ambassador/v0/check_ready"
#              port   = 8877
#              scheme = "HTTP"
#            }
#          }

        }
      termination_grace_period_seconds = 10
	volume {
          name = "varlog"
          host_path {
	  path = "/var/log"
		  }
           }

        volume {
          name = "journal"
          host_path {
	    path = "/var/log/journal"
		  }
           }

        volume {
          name = "varlibdockercontainers"
          host_path {
	    path = "/var/lib/docker/containers"
		  }
           }

        volume {
          name = "fluent-bit-config"
          config_map {
            name = "fluent-bit-config"
          }
        }

      }
    }
  }

}