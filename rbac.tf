

resource "kubernetes_service_account" "fluent-bit" {
  metadata {
    name      = "fluent-bit"
    namespace = var.namespace
  }
}

resource "kubernetes_role" "fluent-bit" {
  metadata {
    name      = "fluent-bit-read"
#    namespace = var.namespace
  }
  rule {
    api_groups = [""]
    resources  = ["namespaces","pods"]
    verbs      = ["get","list","watch"]
  }

}

resource "kubernetes_role_binding" "fluent-bit-read" {
  metadata {
    name      = "fluent-bit-read"
#    namespace = var.namespace

  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_role.fluent-bit.metadata.0.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.fluent-bit.metadata.0.name
    namespace = var.namespace
  }
}
