resource "kubernetes_namespace" "fluent-bit" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_secret" "fluent-bit" {
  metadata {
    name        = "fluent-bit"
    namespace   = var.namespace
  }
  type = "Opaque"
  data = {
    fluentbt_port  = var.fluentbt_port
    fluentbt_host  = var.fluentbt_host
    fluentbt_index  = var.fluentbt_index
    fluentbt_logstash_prefix  = var.fluentbt_logstash_prefix
    fluentbt_type  = var.fluentbt_type
    fluentbt_user  = var.fluentbt_user
    fluentbt_pass  = var.fluentbt_pass

  }
}

