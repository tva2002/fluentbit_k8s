variable "namespace" {
  description = "Kubernetes namespace"
  type        = string
  default     = "kube-logging"
}


variable "image_name" {
  description = "Container image name including registry address. For images from Docker Hub short names can be used"
  type        = string
  default     = "fluent"
}

variable "image_tag" {
  description = "Container image tag (version)"
  type        = string
  default     = "fluent-bit:1.3.8"
}

variable "image_pull_secrets" {
  description = "List of image pull secrets to use with private registries"
  type        = list(string)
  default     = []
}

variable "image_pull_policy" {
  description = "Image pull policy. One of Always, Never or IfNotPresent"
  type        = string
  default     = "Always"
}

variable "fluentbt_port" {
  description = "Elasticsearch port"
  type        = string
  default     = "9200"
}

variable "fluentbt_host" {
  description = "Elasticsearch host"
  type        = string
  default     = "licalhost"
}

variable "fluentbt_index" {
  description = "Fluentbt index"
  type        = string
  default     = "fl_index"
}

variable "fluentbt_logstash_prefix" {
  description = "Fluentbt logstash prefix"
  type        = string
  default     = "fl_prefix"
}


variable "fluentbt_type" {
  description = "Elasticsearch port"
  type        = string
  default     = "_doc"
}

variable "fluentbt_user" {
  description = "Fluentbt user"
  type        = string
  default     = "admin"
}

variable "fluentbt_pass" {
  description = "Fluentbt password"
  type        = string
  default     = "admin"
}