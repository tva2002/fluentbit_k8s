resource "kubernetes_config_map" "fluent-bit" {
  metadata {
    name        = "fluent-bit-config"
    namespace   = var.namespace
   labels ={
     k8s-app = "fluent-bit"
     }
  }


 data = {

"fluent-bit.conf" = "${file("${path.module}/conf/fluent-bit.conf")}"
"filter-kubernetes.conf" = "${file("${path.module}/conf/filter-kubernetes.conf")}"
"input-kubernetes.conf" = "${file("${path.module}/conf/input-kubernetes.conf")}"
"output-elasticsearch.conf" = "${file("${path.module}/conf/output-elasticsearch.conf")}"
"parsers.conf" = "${file("${path.module}/conf/parsers.conf")}"

  }
}